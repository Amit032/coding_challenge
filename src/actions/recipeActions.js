import { GET_RECIPE, ADD_RECIPE, DELETE_RECIPE, EDIT_RECIPE } from './actionTypes';

export const getRecipes = () => {
    console.log('Get Recipe Action Creator Called!!')
    return (dispatch) => { 
        return dispatch({
            type: GET_RECIPE, 
        });
    }
};

export const addRecipe = (newRecipe) => {
    return (dispatch) => { 
        return dispatch({
            type: ADD_RECIPE, 
            payload: newRecipe
        });
    }
};

export const editRecipe = (recipe) => {
    return (dispatch) => { 
        return dispatch({
            type: EDIT_RECIPE, 
            payload: recipe
        });
    }
};

export const deleteRecipe = (recipeId) => {
    console.log('Recipe ID....', recipeId)
    return (dispatch) => { 
        return dispatch({
            type: DELETE_RECIPE,
            payload: recipeId
        });
    }
};
