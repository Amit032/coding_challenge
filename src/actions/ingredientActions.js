import { GET_INGREDIENT, ADD_INGREDIENT } from '../actions/actionTypes';

export const getIngredient = () => {
    console.log('Get Ingredient Action Creator Called!!')
    return (dispatch) => { 
        return dispatch({
            type: GET_INGREDIENT, 
        });
    }
};

export const addIngredient = (newIngredient) => {
    console.log('New Ingredients......', newIngredient)
    return (dispatch) => { 
        return dispatch({
            type: ADD_INGREDIENT, 
            payload: newIngredient
        });
    }
};