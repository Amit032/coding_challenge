// Recipes Constants.
export const GET_RECIPE = 'GET_RECIPE';
export const ADD_RECIPE = 'ADD_RECIPE';
export const DELETE_RECIPE = 'DELETE_RECIPE';
export const EDIT_RECIPE = 'EDIT_RECIPE';

// Ingredient Constants
export const GET_INGREDIENT = 'GET_INGREDIENT';
export const ADD_INGREDIENT = 'ADD_INGREDIENT';