import { GET_INGREDIENT, ADD_INGREDIENT } from '../actions/actionTypes';
import initialState from './initialState';

const ingredients = (state = initialState, action) => {
    switch (action.type) {
        case GET_INGREDIENT:
            console.log('Ingredient Reducer Called!!', state)
            return {    
                ingredientList: state.ingredientList
            };
        case ADD_INGREDIENT:
            console.log('New Ingredients payload......', action.payload)
            return {
                ...state,
                ingredientList: state.ingredientList.concat(action.payload),
            };
        default:
            return state;
    }
}

export default ingredients;