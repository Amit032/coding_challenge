import { combineReducers } from 'redux';
import recipes from './recipeReducers';
import ingredients from './ingredientReducer'
import { reducer as reducerForm } from 'redux-form';

const rootReducer = combineReducers({
  form: reducerForm,
  ingredients,
  recipes,
})
export default rootReducer;