import { GET_RECIPE, ADD_RECIPE, DELETE_RECIPE, EDIT_RECIPE } from '../actions/actionTypes';
import initialState from './initialState';

const items = (state = initialState, action) => {
    switch (action.type) {
        case GET_RECIPE:
            console.log('Get Recipe Reducer Called!!', state)
            return {    
                recipesList: state.recipesList
            }; 
        case ADD_RECIPE:
            return {
                ...state,
                recipesList: state.recipesList.concat(action.payload),
            };
        case EDIT_RECIPE:    
            return {    
                ...state,    
                recipesList: state.recipesList.map(    
                    (content, i) => content.id === action.payload.id ? {...content, dishName : action.payload.dishName }    
                                            : content)    
            };
        case DELETE_RECIPE:
            console.log(state.recipesList)  
            console.log(action.payload)
            return {    
                ...state,    
                recipesList: state.recipesList.filter(item => item.id !== action.payload)    
            };
        default:
            return state;
    }
}

export default items;