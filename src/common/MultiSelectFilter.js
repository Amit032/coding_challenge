import React from 'react'
import PropTypes from 'prop-types'
// Material UI AutoComplete Component
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const MultiSelectFilter = props => {
    const { ingredientList, getSelectedIngredient } = props;
    console.log(ingredientList)
    const getSelectedOption = (selectedIngredients) => {
        getSelectedIngredient(selectedIngredients)
    }

    return (
        <Autocomplete
            multiple
            limitTags={2}
            id="checkboxes-tags-demo"
            size="small"
            options={ingredientList}
            disableCloseOnSelect
            getOptionLabel={(option) => option.name || ''}
            onChange={(event, value) => getSelectedOption(value)}
            renderOption={(option, { selected }) => (
            <React.Fragment>
                <Checkbox
                icon={icon}
                checkedIcon={checkedIcon}
                style={{ marginRight: 8 }}
                checked={selected}
                />
                {option.name}
            </React.Fragment>
            )}
            style={{ width: 500 }}
            renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Filter by ingredients" placeholder="Favorites" />
            )}
      />
    )
}

MultiSelectFilter.propTypes = {
    ingredientList: PropTypes.array,
    getSelectedIngredient: PropTypes.func
}

export default MultiSelectFilter

