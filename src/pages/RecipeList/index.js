import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// Material UI Components
import { withStyles } from '@material-ui/core/styles';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
// Material UI Icons
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add'

import MultiSelectFilter from '../../common/MultiSelectFilter'
import { getRecipes, deleteRecipe, getIngredient } from '../../actions'

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  button: {
    margin: theme.spacing.unit,
  }
});

const RecipeList = props => {
  const { recipesList: recipes, ingredientList: ingredients, classes } = props;

  const [filteredList, setFilteredList] = useState(recipes)
  const [ingredientList, setIngredientList] = useState(ingredients)

  useEffect(() => {
    props.getRecipes();
    props.getIngredient();
    // Get Data from local storage for initial render
    // var recipesFromLocalStorage = localStorage.getItem('recipes');
    // recipesFromLocalStorage = JSON.parse(recipesFromLocalStorage);
    // var ingredientFromLocalStorage = localStorage.getItem('ingredients');
    // ingredientFromLocalStorage = JSON.parse(ingredientFromLocalStorage)

    // setIngredientList(ingredientFromLocalStorage)
    // setFilteredList(recipesFromLocalStorage)
  }, [])

  useEffect(() => {
    // Get Data from local storage and parse it
    // var ingredientFromLocalStorage = localStorage.getItem('ingredients');
    // ingredientFromLocalStorage = JSON.parse(ingredientFromLocalStorage);
    setIngredientList(ingredients)
  }, [ingredients])

  useEffect(() => {
    // Get Data from local storage and parse it
    // var recipesFromLocalStorage = localStorage.getItem('recipes');
    // recipesFromLocalStorage = JSON.parse(recipesFromLocalStorage);
    setFilteredList(recipes)
  }, [recipes])

  const editRecipeDetails = (data) => {
    window.confirm("Clicked on Edit button!!")
  }

  const deleteRecipe = (id) => {
    if (window.confirm("Are you sure?")) {  
      props.deleteRecipe(id);  
    }
  }

  const addNewRecipe = () => {
    props.history.push(`/addNewRecipe`);
  }

  // Filter List by Selected Ingredients
  const getSelectedIngredient = (selectedIngredients) => {

    const filteredList = recipes.filter(recipe => 
      recipe?.ingredient?.find(obj => 
        selectedIngredients.some(nameObj => nameObj.name === obj.ingredientName)
      ))
    
    // Check if there is no value in filter Array
    filteredList.length >= 1 ? setFilteredList(filteredList) : setFilteredList(recipes)
  }


  return (
    <div style={{ padding: 70 }}>
      <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingTop: 50, paddingBottom: 30 }}>
        <Button variant="contained" mini color="primary" aria-label="addd" className={classes.button} onClick={addNewRecipe}>
          <AddIcon />
        </Button>
        <MultiSelectFilter ingredientList={ingredientList} getSelectedIngredient={getSelectedIngredient} />
      </div>
      {
        filteredList && filteredList.length >= 1 && (
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <CustomTableCell>Dish Name</CustomTableCell>
                  <CustomTableCell align="center">Edit</CustomTableCell>
                  <CustomTableCell align="center">Delete</CustomTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {filteredList && filteredList.map((recipeDetails, index) => {
                  return (
                    <TableRow className={classes.row} key={`${recipeDetails}${index}`}>
                      <CustomTableCell>{recipeDetails.dishName}</CustomTableCell>
                      <CustomTableCell align="center">
                        <Button variant="fab" mini aria-label="edit" className={classes.button} onClick={() => editRecipeDetails(recipeDetails)}>
                          <EditIcon />
                        </Button>
                      </CustomTableCell>
                      <CustomTableCell align="center">
                        <Button variant="fab" mini color="secondary" aria-label="delete" className={classes.button} onClick={() => deleteRecipe(recipeDetails.id)} >
                          <DeleteIcon />
                        </Button>
                      </CustomTableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        )
      }
      {
        filteredList.length === 0 && (
          <h1 style={{ textAlign: 'center', marginTop: '15%' }}>Please add a Recipe!!</h1>
        )
      }
    </div>
  )
}

RecipeList.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    recipesList: state.recipes.recipesList,
    ingredientList: state.ingredients.ingredientList
  }
}

export default connect(mapStateToProps, { getRecipes, deleteRecipe, getIngredient } )(withStyles(styles)(RecipeList));
