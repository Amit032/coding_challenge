import React from 'react';
import PropTypes from 'prop-types';
import RecipeForm from './RecipeForm';
import { connect } from 'react-redux';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import * as actionTypes from '../../actions';

const AddNewRecipe = props => {
    const { addRecipe, addIngredient, history } = props

    const saveRecipeInfo = async (values) => {
        // Add a dynamic id key to delete or edit a particular Recipe
        values["id"] = Math.floor(Math.random() * 100);

        const filterIngredient = await values?.ingredient.map((obj) => ({name: obj.ingredientName}))
        var oldRecipes = localStorage.getItem('recipes');
        var oldIngredients = localStorage.getItem('ingredients');
        // Store newly added Recipe in local storage
        if (oldRecipes === null) {
            var recipeArr = [values];
            localStorage.setItem('recipes', JSON.stringify(recipeArr));
        } else {
            oldRecipes = JSON.parse(oldRecipes);
            localStorage.setItem('recipes', JSON.stringify(oldRecipes.concat(values)));
        }

        // Store ingredients in local storage
        if (oldIngredients === null) {
            var ingredArr = [filterIngredient];
            localStorage.setItem('ingredients', JSON.stringify(ingredArr));
        } else {
            oldIngredients = JSON.parse(oldIngredients);
            localStorage.setItem('ingredients', JSON.stringify(oldIngredients.concat(filterIngredient)));
        }
        addIngredient(filterIngredient)
        addRecipe(values)
        history.push(`/recipesList`);
    }

    const redirectToRecipeList = () => {
        history.push(`/recipesList`);
    }

    return (
        <div style={{ padding: 20, marginTop: '5%' }}>
            <Card>
                <CardContent>
                    <h1>Add New Recipe</h1>
                </CardContent>
                <RecipeForm
                    onSubmit={saveRecipeInfo}
                    onCancel={redirectToRecipeList}
                />
            </Card>
        </div>
    )
}

const mapDispatchToProps = dispatch => {
    return {
      addRecipe: (value) => dispatch(actionTypes.addRecipe(value)),
      addIngredient: (value) => dispatch(actionTypes.addIngredient(value))
    }
}

AddNewRecipe.propTypes = {
    dispatch: PropTypes.func
};

AddNewRecipe.contextTypes = {
    router: PropTypes.object
};

export default connect(null, mapDispatchToProps)(AddNewRecipe);
