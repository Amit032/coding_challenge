import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { Field, FieldArray, reduxForm } from 'redux-form'
import { connect } from 'react-redux';
import validate from './formValidation'
import { styles } from './styles'

const RecipeForm = props => {
  const { handleSubmit, reset, pristine, submitting, classes, onCancel } = props

  const renderField = ({ input, label, meta: { touched, error } }) => {
    return (
      <div className={classes.fieldContainer}>
        <FormControl variant="outlined" className={classes.inputStyle}>
          <InputLabel htmlFor="component-outlined">{label}</InputLabel>
          <OutlinedInput {...input} id="component-outlined" value={`${input.value}`} label="Name" />
          {touched && error && <span className={classes.errMsg}>{error}</span>}
        </FormControl>
      </div>
    )
  }

  const renderQuantity = ({ input, label, meta: { touched, error } }) => (
    <FormControl className={classes.formControl}>
      <InputLabel id="demo-simple-select-label">Qty</InputLabel>
      <Select
        {...input}
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        onChange={value => input.onChange(value)}
        >
        <MenuItem value={'1 kg.'}>1 kg.</MenuItem>
        <MenuItem value={'500 gm.'}>500 gm.</MenuItem>
        <MenuItem value={'100 gm.'}>100 gm.</MenuItem>
        <MenuItem value={'50 gm.'}>50 gm.</MenuItem>
      </Select>
      {touched && error && <span className={classes.errMsg}>{error}</span>}
    </FormControl>
  )

  const renderIngredient = ({ fields, meta: { touched, error, submitFailed } }) => (
    <ul className={classes.ingredientContainer}>
      <div>
        <Button 
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => fields.push({})}
        >
          Add Ingredient
        </Button>
        {submitFailed && error && <span className={classes.errMsg}>{error}</span>}
      </div>
      {fields.map((ingredient, index) => (
        <Card key={index} className={classes.ingredientCard}>
          <div className={classes.ingredient}>
            <CardContent>Ingredient #{index + 1}</CardContent>
            <IconButton 
              title="Remove Ingredient"
              color="secondary"
              aria-label="addd"
              component="span"
              onClick={() => fields.remove(index)}
            >
              <DeleteIcon fontSize="large" />
            </IconButton>
          </div>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <Field
              name={`${ingredient}.ingredientName`}
              type="text"
              component={renderField}
              label="Ingredient Name"
            />
            <Field 
              name={`${ingredient}.quantity`}
              type="text"
              component={renderQuantity}
              label="Qty"
            />
          </div>
          <FieldArray name={`${ingredient}.steps`} component={renderCookSteps} />
        </Card>
      ))}
    </ul>
  )

  const renderCookSteps = ({ fields, meta: { error } }) => (
    <ul style={{ paddingInlineStart: 0  }}>
      <div className={classes.cookStep}>
        <Button variant="contained" color="primary" className={classes.button} onClick={() => fields.push({})}>
            Add Steps to Cook
        </Button>
      </div>
      {fields.map((Step, index) => (
        <ul key={index} className={classes.cookInput}>
          <Field
            name={`${Step}.step ${index + 1}`}
            type="text"
            component={renderField}
            label={`Step #${index + 1}`}
          />
          <IconButton 
            title="Remove Ingredient"
            color="secondary"
            aria-label="addd"
            component="span"
            onClick={() => fields.remove(index)}
          >
            <DeleteIcon fontSize="large" />
          </IconButton>
        </ul>
      ))}
      {error && <li className="error">{error}</li>}
    </ul>
  )

  return (
    <form onSubmit={handleSubmit}>
      <div className={classes.container}>
        <Field
          name="dishName"
          type="text"
          component={renderField}
          className={classes.textField}
          margin="normal"
          label="Dish Name"
        />
        <Field
          name="pictureOfDish"
          type="text"
          component={renderField}
          className={classes.textField}
          margin="normal"
          label="Picture of Dish in URL format"
        />
        <FieldArray name="ingredient" component={renderIngredient} />
      </div>
      <div className={classes.btnContainer}>
        <Button variant="contained" type="submit" color="primary" className={classes.button} disabled={submitting}>
          Save
        </Button>
        <Button variant="contained" color="inherit" className={classes.button} disabled={pristine || submitting} onClick={reset}>
          Clear Values
        </Button>
        <Button variant="contained" color="secondary" className={classes.button} onClick={onCancel}>
          Cancel
        </Button>
      </div>
    </form>
  )
}

RecipeForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default reduxForm({
  form: 'recipeForm',  // a unique identifier for this form
  validate
})(
  connect(null)(withStyles(styles)(RecipeForm))
)

