const validate = values => {
    const errors = {}
    if (!values.dishName) {
      errors.dishName = '*Required'
    }
    if (!values.pictureOfDish) {
      errors.pictureOfDish = '*Required'
    }
    if (!values.ingredient || !values.ingredient.length) {
      errors.ingredient = { _error: 'At least one Ingredient must be entered' }
    } else {
      const ingredientArrayErrors = []
      values.ingredient.forEach((ingredient, ingredientIndex) => {
        const ingredientErrors = {}
        if (!ingredient || !ingredient.ingredientName) {
          ingredientErrors.ingredientName = '*Required'
          ingredientArrayErrors[ingredientIndex] = ingredientErrors
        }
        if (!ingredient || !ingredient.quantity) {
          ingredientErrors.quantity = '*Required'
          ingredientArrayErrors[ingredientIndex] = ingredientErrors
        }
        if (ingredient && ingredient.steps && ingredient.steps.length) {
          const stepsArrayErrors = []
          ingredient.steps.forEach((steps, stepsIndex) => {
            if (!steps || !steps.length) {
              stepsArrayErrors[stepsIndex] = '*Required'
            }
          })
          if (stepsArrayErrors.length) {
            ingredientErrors.steps = stepsArrayErrors
            ingredientArrayErrors[ingredientIndex] = ingredientErrors
          }
          if (ingredient.steps.length > 5) {
            if (!ingredientErrors.steps) {
              ingredientErrors.steps = []
            }
            ingredientErrors.steps._error = 'No more than five steps allowed'
            ingredientArrayErrors[ingredientIndex] = ingredientErrors
          }
        }
      })
      if (ingredientArrayErrors.length) {
        errors.ingredient = ingredientArrayErrors
      }
    }
    return errors
  }

  export default validate;