export const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'baseline',
      padding: '0px 20px'
    },
    fieldContainer: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center'
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 250,
    },
    inputStyle: {
      display: 'flex',
      padding: 10,
      width: '80%' 
    },
    ingredientContainer: {
      width: '88%',
      paddingInlineStart: 0 
    },
    ingredientCard: {
      width: '89%',
      padding: 25,
      margin: '10px 8px' 
    },
    ingredient: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    cookStep: {
      display: 'flex',
      justifyContent: 'center' 
    },
    cookInput: {
      display: 'flex',
      alignItems: 'center',
      paddingInlineStart: 0
    },
    button: {
      margin: theme.spacing.unit,
    },
    errMsg: {
        fontWeight: 'bold',
        color: 'red'
    },
    btnContainer: {
        display: 'flex',
        justifyContent: 'center',
        paddingTop: 100
    }
  });