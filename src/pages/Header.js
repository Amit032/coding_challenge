import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1,
  }
};

const Header = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{...style.appBar}} >
        <Toolbar variant="dense">
          <Typography variant="h4" color="inherit">
            Coding Challenge
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  )
}

const style = {
  appBar: {
    position: 'fixed',
    top: 0,
    height: 70,
    fontSize: 32,
    fontWeight: 'bold',
    alignItems: 'center',
  },
};

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);

