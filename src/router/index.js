import React from 'react';
import AddNewRecipe from '../pages/AddNewRecipe';
import RecipeList from '../pages/RecipeList';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import App from '../App';


class Index extends React.Component {
  render() {
    return (
      <Router>
        <App>
          <Switch>
            <Route exact path="/" component={RecipeList} />
            <Route exact path="/recipesList" component={RecipeList} />
            <Route exact path="/addNewRecipe" component={AddNewRecipe} />
          </Switch>
        </App>
      </Router>
    );
  }
}
export default Index;