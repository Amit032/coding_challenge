import React, { Component } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import cyan from '@material-ui/core/colors/cyan';
import green from '@material-ui/core/colors/green';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './pages/Header';
//import Footer from './pages/Footer';
// A theme with custom primary and secondary color.
// It's optional.


const theme = createMuiTheme({
  palette: {
    primary: {
      light: cyan[300],
      main: cyan[500],
      dark: cyan[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
    },
  },
});

class App extends Component {

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <div>
          <Header />
          <div>
            {this.props.children}
          </div>
        </div>

      </MuiThemeProvider>
    );
  }
}

export default withWidth()(App);
